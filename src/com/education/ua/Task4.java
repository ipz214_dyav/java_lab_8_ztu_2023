package com.education.ua;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Task4 {
    public static void main(String[] args) {
        Product[] products = {
                new Product("Product1", "Brand1", 10.99, 5),
                new Product("Product2", "Brand2", 19.99, 8),
                new Product("Product3", "Brand3", 15.49, 3),
                new Product("Product4", "Brand3", 15.49, 3),

        };

        Stream<Product> productStream = Arrays.stream(products);

        productStream.map(Product::getBrand).forEach(System.out::println);

        Stream<Product> filteredStream = Arrays.stream(products)
                .filter(product -> product.getPrice() < 1000)
                .limit(2);

        filteredStream.map(Product::getName).forEach(System.out::println);

        int totalItemsInStock = Arrays.stream(products)
                .mapToInt(Product::getCount)
                .reduce(0, Integer::sum);

        System.out.println("���� ��� ������ �� �����: " + totalItemsInStock);

        Map<String, List<Product>> productsByBrand = Arrays.stream(products).collect(Collectors.groupingBy(Product::getBrand));

        productsByBrand.forEach((brand, productList) -> {
            System.out.println("�����: " + brand);
            productList.forEach(product -> System.out.println("  �����: " + product.getName()));
        });

        Product[] sortedProducts = Arrays.stream(products)
                .sorted(Comparator.comparingDouble(Product::getPrice))
                .toArray(Product[]::new);


        Arrays.stream(sortedProducts)
                .forEach(product -> System.out.println("Name: " + product.getName() + ", Price: " + product.getPrice()));

    }
}
